using System;

namespace Sauron
{
    internal class GenericObserver<T>: IObserver<T>
    {
        private IDisposable unsubscriber;

        private Action<T> ReceiveFunc { get; }
        private Action<Exception> OnErrorFunc { get; }
        private Action OnCompleteFunc { get; }

        public GenericObserver(Action<T> ReceiveFunc, Action<Exception> OnErrorFunc, Action OnCompleteFunc)
        {
            this.ReceiveFunc = ReceiveFunc;
            this.OnErrorFunc = OnErrorFunc;
            this.OnCompleteFunc = OnCompleteFunc;
        }

        public void Subscribe(GenericProvider<T> provider)
        {
            if(provider != null)
            {
                unsubscriber = provider.Subscribe(this);
            }
        }
        public void OnCompleted() => this.OnCompleteFunc();
        public void OnError(Exception error) => this.OnError(error);
        public void OnNext(T value) => this.ReceiveFunc(value);
    }
}