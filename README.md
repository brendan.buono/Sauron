# Overview
Sauron is a stateless Observer system that uses the IObservable interface within .NET to emit immutable data moving within a system.

# TODO
* Implement `Sauron` class, acts as a bus
* Implement generic tracker -- done
* Application crash handling


# Design
Many event systems (including the one provided by C#) allow loose couple between objects, however, that ends up with tightly coupled systems. Sauron attemptes to solve this by allowing loose coupling between data instead. Components register to listen when data is emitted and act upon the information within the data, instead of an event taking place within the business logic.