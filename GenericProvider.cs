using System;
using System.Collections.Generic;

namespace Sauron
{
    internal class GenericProvider<T> : IObservable<T>
    {
        List<IObserver<T>> observers = new();
        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
            return new Unsubscriber(this.observers, observer);
        }

        public void FireEvent(T obj) => observers.ForEach(o => o.OnNext(obj));
        public void Shutdown() => observers.ForEach(o => o.OnCompleted());
        private class Unsubscriber : IDisposable
        {
            private readonly List<IObserver<T>> observers;
            private readonly IObserver<T> observer;
            public Unsubscriber(List<IObserver<T>> observers, IObserver<T> observer)
            {
                this.observers = observers;
                this.observer = observer;
            }

            public void Dispose()
            {
                if (observers is not null && observers.Contains(observer))
                {
                    observers.Remove(observer);
                }
            }
        }
    }
}